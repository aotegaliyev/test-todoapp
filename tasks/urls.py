from django.urls import path, include
from tasks.views import (
    TaskList,
    TaskDetail,
)


urlpatterns = [
    path("api/v1/tasks", TaskList.as_view(), name='index'),
    path("api/v1/tasks/<int:pk>", TaskDetail.as_view(), name='detail'),
]